object NewContactForm: TNewContactForm
  Left = 243
  Top = 108
  BorderStyle = bsDialog
  ClientHeight = 367
  ClientWidth = 290
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clBlack
  Font.Height = -13
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  Position = poMainFormCenter
  OnShow = FormShow
  PixelsPerInch = 96
  TextHeight = 17
  object StaticText1: TStaticText
    Left = 25
    Top = 10
    Width = 57
    Height = 21
    Caption = #1060#1072#1084#1080#1083#1080#1103
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentFont = False
    TabOrder = 0
  end
  object FamilyMaskEdit: TMaskEdit
    Left = 24
    Top = 26
    Width = 235
    Height = 25
    TabOrder = 1
    Text = 'FamilyMaskEdit'
  end
  object StaticText2: TStaticText
    Left = 25
    Top = 55
    Width = 30
    Height = 21
    Caption = #1048#1084#1103
    TabOrder = 2
  end
  object NameMaskEdit: TMaskEdit
    Left = 24
    Top = 71
    Width = 235
    Height = 25
    TabOrder = 3
    Text = 'NameMaskEdit'
  end
  object StaticText3: TStaticText
    Left = 25
    Top = 100
    Width = 96
    Height = 21
    Caption = #1044#1072#1090#1072' '#1088#1086#1078#1076#1077#1085#1080#1103
    TabOrder = 4
  end
  object DataMaskEdit: TMaskEdit
    Left = 24
    Top = 116
    Width = 235
    Height = 25
    EditMask = '!99/99/00;1;_'
    MaxLength = 8
    TabOrder = 5
    Text = '  .  .  '
  end
  object StaticText4: TStaticText
    Left = 25
    Top = 145
    Width = 156
    Height = 21
    Caption = #1050#1086#1084#1087#1072#1085#1080#1103'/'#1052#1077#1089#1090#1086' '#1088#1072#1073#1086#1090#1099
    TabOrder = 6
  end
  object CompanyMaskEdit: TMaskEdit
    Left = 24
    Top = 161
    Width = 235
    Height = 25
    TabOrder = 7
    Text = 'CompanyMaskEdit'
  end
  object StaticText5: TStaticText
    Left = 25
    Top = 190
    Width = 87
    Height = 21
    Caption = #1069#1083#1077#1082#1090#1088#1086#1087#1086#1095#1090#1072
    TabOrder = 8
  end
  object EmailMaskEdit: TMaskEdit
    Left = 24
    Top = 206
    Width = 235
    Height = 25
    TabOrder = 9
    Text = 'EmailMaskEdit'
  end
  object StaticText6: TStaticText
    Left = 24
    Top = 235
    Width = 56
    Height = 21
    Caption = #1058#1077#1083#1077#1092#1086#1085
    TabOrder = 10
  end
  object PhoneMaskEdit: TMaskEdit
    Left = 25
    Top = 251
    Width = 235
    Height = 25
    EditMask = '+7!\(999\)000-00-00;1;_'
    MaxLength = 16
    TabOrder = 11
    Text = '+7(   )   -  -  '
  end
  object StaticText7: TStaticText
    Left = 25
    Top = 280
    Width = 80
    Height = 21
    Caption = #1042' '#1080#1079#1073#1088#1072#1085#1085#1099#1093
    TabOrder = 12
  end
  object FavoriteCheckBox: TCheckBox
    Left = 110
    Top = 282
    Width = 18
    Height = 18
    TabOrder = 13
  end
  object SaveContactBitBtn: TBitBtn
    Left = 179
    Top = 321
    Width = 80
    Height = 25
    Hint = #1057#1086#1093#1088#1072#1085#1080#1090#1100' '#1080#1079#1084#1077#1085#1077#1085#1080#1103
    Caption = #1057#1086#1093#1088#1072#1085#1080#1090#1100
    TabOrder = 14
    OnClick = SaveContactBitBtnClick
  end
  object DeleteContactBitBtn: TBitBtn
    Left = 25
    Top = 321
    Width = 80
    Height = 25
    Hint = #1059#1076#1072#1083#1080#1090#1100' '#1082#1086#1085#1090#1072#1082#1090
    Caption = #1059#1076#1072#1083#1080#1090#1100
    TabOrder = 15
    OnClick = DeleteContactBitBtnClick
  end
end
