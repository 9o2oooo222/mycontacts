object MainForm: TMainForm
  Left = 197
  Top = 111
  BorderIcons = [biSystemMenu, biMinimize]
  BorderStyle = bsSingle
  Caption = #1052#1086#1080' '#1082#1086#1085#1090#1072#1082#1090#1099
  ClientHeight = 577
  ClientWidth = 540
  Color = clBtnFace
  Font.Charset = RUSSIAN_CHARSET
  Font.Color = clWindowText
  Font.Height = -13
  Font.Name = 'Segoe UI'
  Font.Style = []
  OldCreateOrder = False
  PopupMode = pmAuto
  Position = poDesktopCenter
  OnCreate = FormCreate
  OnDestroy = FormDestroy
  PixelsPerInch = 96
  TextHeight = 17
  object StaticText2: TStaticText
    Left = 10
    Top = 31
    Width = 63
    Height = 21
    Caption = #1060#1072#1084#1080#1083#1080#1103
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 5
  end
  object StaticText3: TStaticText
    Left = 137
    Top = 31
    Width = 31
    Height = 21
    Caption = #1048#1084#1103
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 6
  end
  object StaticText4: TStaticText
    Left = 264
    Top = 31
    Width = 93
    Height = 21
    Caption = #1069#1083#1077#1082#1090#1088#1086#1087#1086#1095#1090#1072
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 7
  end
  object StaticText5: TStaticText
    Left = 391
    Top = 31
    Width = 60
    Height = 21
    Caption = #1058#1077#1083#1077#1092#1086#1085
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = [fsBold]
    ParentFont = False
    TabOrder = 9
  end
  object StatusBar: TStatusBar
    Left = 0
    Top = 558
    Width = 540
    Height = 19
    Margins.Left = 2
    Margins.Top = 2
    Margins.Right = 2
    Margins.Bottom = 2
    AutoHint = True
    Panels = <>
    SimplePanel = True
  end
  object StringGrid1: TStringGrid
    Left = 1
    Top = 49
    Width = 530
    Height = 478
    BevelInner = bvNone
    BevelOuter = bvNone
    Color = clBtnFace
    DefaultColWidth = 429
    FixedCols = 0
    RowCount = 1
    FixedRows = 0
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -12
    Font.Name = 'Segoe UI'
    Font.Style = []
    Options = [goFixedVertLine, goFixedHorzLine, goVertLine, goHorzLine, goRangeSelect, goRowSelect]
    ParentFont = False
    ScrollBars = ssVertical
    TabOrder = 0
    OnDblClick = EditContactClick
    OnSelectCell = StringGrid1SelectCell
  end
  object NewContactBitBtn: TBitBtn
    Left = 1
    Top = 1
    Width = 107
    Height = 25
    Hint = #1057#1086#1079#1076#1072#1090#1100' '#1085#1086#1074#1099#1081' '#1082#1086#1085#1090#1072#1082#1090
    Caption = #1053#1086#1074#1099#1081' '#1082#1086#1085#1090#1072#1082#1090
    Style = bsNew
    TabOrder = 1
    OnClick = NewContactBitBtnClick
  end
  object ShowFavoritesBitBtn: TBitBtn
    Left = 431
    Top = 0
    Width = 99
    Height = 25
    Hint = #1055#1086#1082#1072#1079#1072#1090#1100' '#1090#1086#1083#1100#1082#1086' '#1080#1079#1073#1088#1072#1085#1085#1099#1077' '#1082#1086#1085#1090#1072#1082#1090#1099
    Caption = #1048#1079#1073#1088#1072#1085#1085#1099#1077
    TabOrder = 2
    OnClick = ShowFavoritesBitBtnClick
  end
  object SearchComboBox: TComboBox
    Left = 68
    Top = 526
    Width = 113
    Height = 25
    Style = csDropDownList
    ItemIndex = 0
    TabOrder = 3
    Text = #1060#1072#1084#1080#1083#1080#1103
    OnChange = SearchComboBoxChange
    Items.Strings = (
      #1060#1072#1084#1080#1083#1080#1103
      #1048#1084#1103
      #1050#1086#1084#1087#1072#1085#1080#1103
      #1069#1083#1077#1082#1090#1088#1086#1087#1086#1095#1090#1072
      #1058#1077#1083#1077#1092#1086#1085)
  end
  object StaticText1: TStaticText
    Left = 0
    Top = 529
    Width = 62
    Height = 21
    Caption = #1055#1086#1080#1089#1082' '#1087#1086':'
    TabOrder = 4
  end
  object SearchResultText: TStaticText
    Left = 213
    Top = 272
    Width = 117
    Height = 21
    Caption = #1085#1080#1095#1077#1075#1086' '#1085#1077' '#1085#1072#1081#1076#1077#1085#1086
    Color = clBtnFace
    Font.Charset = RUSSIAN_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Segoe UI'
    Font.Style = []
    ParentColor = False
    ParentFont = False
    TabOrder = 10
    Visible = False
  end
  object SearchContactBox: TMaskEdit
    Left = 180
    Top = 526
    Width = 351
    Height = 25
    TabOrder = 11
    Text = ''
    TextHint = #1089#1090#1088#1086#1082#1072' '#1087#1086#1080#1089#1082#1072
    OnChange = SearchContactBoxChange
  end
end
