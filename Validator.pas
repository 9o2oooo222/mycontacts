unit Validator;

interface
uses System.RegularExpressions, System.DateUtils, System.SysUtils;

const
  RussianChars = ['�'..'�','�'..'�','�','�'];
  LatinChars = ['A'..'Z','a'..'z'];
  NumbersChars = ['0'..'9'];

type
 TValidator = class(TObject)
   constructor Create;
   destructor Destroy; override;
   function ValidateEmail(Email: string): boolean;
   function ValidateDate(Date: string): boolean;
   function ValidatePhone(Phone: string): boolean;
   function ValidateNames(Name: string): boolean;
   function ValidateCompany(Company: string): boolean;
 end;

implementation

{ TValidator }
//------------------------------------------------------------------------------
constructor TValidator.Create;
begin
  inherited Create;
end;
//------------------------------------------------------------------------------
destructor TValidator.Destroy;
begin
  inherited Destroy;
end;
//------------------------------------------------------------------------------
// �������� �� ���������� �������� �������� �������� ��������� �������
function TValidator.ValidateCompany(Company: string): boolean;
var index : integer;
    flag  : boolean;
    _Company : AnsiString;
begin
  Result:= false;
  if (Company <> '') then begin
    _Company:= AnsiString(Company);
    flag:= true;
    index:= 1;
    while (index <= Length(_Company)) and flag do begin
      if (_Company[index] in LatinChars) or
         (_Company[index] in RussianChars) or
         (_Company[index] in NumbersChars) or
         (_Company[index] in [' ','"']) then flag:= true
                                        else flag:= false;
      Inc(index);
    end;
    Result:= flag;
  end;
end;
//------------------------------------------------------------------------------
// �������� �� ���������� ����
function TValidator.ValidateDate(Date: string): boolean;
begin
  Result:= false;
  if (Date <> '  .  .  ') and (Pos(' ', Date) = 0) then Result:= IsValidDate(StrToInt(Copy(Date,7,2)),StrToInt(Copy(Date,4,2)),StrToInt(Copy(Date,1,2)))
end;
//------------------------------------------------------------------------------
// �������� �� ���������� ������ �������� ��������� �������
function TValidator.ValidateEmail(Email: string): boolean;
var RegEx: TRegEx;
begin
  Result:= false;
  if (EMail <> '') then begin
    RegEx:= TRegEx.Create('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]*[a-zA-Z0-9]+$');
    Result:=  RegEx.Match(EMail).Success;
  end;
end;
//------------------------------------------------------------------------------
// �������� �� ���������� ����� � ������� �������� ��������� �������
function TValidator.ValidateNames(Name: string): boolean;
var index : integer;
    flag  : boolean;
    _Name : AnsiString;
begin
  Result:= false;
  if (Name <> '') then begin
    _Name:= AnsiString(Name);
    flag:= true;
    index:= 1;
    while (index <= Length(_Name)) and flag do begin
      if (_Name[index] in LatinChars) or (_Name[index] in RussianChars) then flag:= true
                                                                        else flag:= false;
      Inc(index);
    end;
    Result:= flag;
  end;
end;
//------------------------------------------------------------------------------
// �������� �� ���������� ������ ��������
function TValidator.ValidatePhone(Phone: string): boolean;
begin
  Result:= false;
  if (Phone <> '+7(   )   -  -  ') then Result:= (Pos(' ', Phone) = 0)
end;
//------------------------------------------------------------------------------
end.
