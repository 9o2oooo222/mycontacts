unit Contacts;

interface
uses System.Classes, System.SysUtils;

type
  PContact = ^TContact;
  TContact = record
    Id        : integer;
    Family    : string;
    Name      : string;
    DateBirth : string;
    Company   : string;
    Email     : string;
    Phone     : string;
    Favorite  : boolean;
    Deleted   : boolean;
  end;

  TContacts = class(TObject)
  private
    FullContactList: TList;
    ContactListToScreen: TList;
    ContactListFilter: TContact;
    function GetNextId: integer;
    function Copy(fromContact: PContact): PContact;
    procedure ClearFullContactList;
    procedure ClearContactListToScreen;
  public
    constructor Create;
    destructor Destroy; override;
    function ItemById(id: integer): PContact;
    function ItemToScreen(index: integer): PContact;
    function Insert(node: TContact): PContact;
    function Count: integer;
    procedure Add;
    procedure UpdateContact(node: TContact);
    procedure UpdateListToScreen;
    procedure Delete(id: integer);
    procedure SortContactList;
    procedure SetContactListFilter(_Family,_Name,_Company,_Email,_Phone: string; _Favorite: boolean);
  end;

implementation
//------------------------------------------------------------------------------
// constructor initialization of variables
constructor TContacts.Create;
begin
  inherited Create;
  FullContactList:= TList.Create;
  ContactListToScreen:= TList.Create;
end;
//------------------------------------------------------------------------------
// adding a contact to the list
procedure TContacts.Add;
var _Record       : PContact;
    s100, s10, s1 : integer;
begin
  new(_Record);
  with _Record^ do begin
    id:= GetNextId;
    Family:=    'Family' + IntToStr(id);
    Name:=      'Name' + IntToStr(id);
    DateBirth:= '01.01.0' + IntToStr(id);
    Company:=   'Company' + IntToStr(id);
    Email:=     'Email' + IntToStr(id) + '@mail.ru';
    s100:= id div 100;
    s10:= (id - s100 * 100) div 10;
    s1:= id mod 10;
    Phone:=     '+7(900)100-0' + IntToStr(s100) + '-' + IntToStr(s10) + IntToStr(s1);
    Favorite:=  Random(4) = 1;
    Deleted:=   false;
  end;
  FullContactList.Add(_Record);
end;
//------------------------------------------------------------------------------
// marking a contact as deleted
procedure TContacts.Delete(id: integer);
var _Record: PContact;
begin
  _Record:= ItemById(id);
  _Record^.Deleted:= true;
end;
//------------------------------------------------------------------------------
// returns the number of contacts in the list
function TContacts.Count: integer;
begin
  Result:= ContactListToScreen.Count
end;
//------------------------------------------------------------------------------
// destructor clear memory
destructor TContacts.Destroy;
var index   : integer;
    _Record : PContact;
begin
  ClearContactListToScreen;
  ClearFullContactList;
  inherited Destroy;
end;
//------------------------------------------------------------------------------
// get a unique id from the contact list
function TContacts.GetNextId: integer;
var index, MaxId : integer;
    _Record      : PContact;
begin
  MaxId:= 0;
  for index := 0 to FullContactList.Count - 1 do begin
    _Record:= FullContactList.Items[index];
    if _Record^.Id > MaxId then MaxId:= _Record^.Id;
  end;
  Result:= MaxId + 1;
end;
//------------------------------------------------------------------------------
// adding a contact to the list in alphabetical order
function TContacts.Insert(node: TContact): PContact;
var _Record : PContact;
    index   : integer;
    flag    : boolean;
begin
  index:= 0;
  flag:= true;
  while (index < FullContactList.Count) and flag do begin
    _Record:= FullContactList.Items[index];
    if _Record^.Family > node.Family then flag:= false
                                     else Inc(index);
  end;

  new(_Record);
  with _Record^ do begin
    id:=        GetNextId;
    Family:=    node.Family;
    Name:=      node.Name;
    DateBirth:= node.DateBirth;
    Company:=   node.Company;
    Email:=     node.Email;
    Phone:=     node.Phone;
    Favorite:=  node.Favorite;
    Deleted:=   false;
  end;

  if flag then FullContactList.Add(_Record)
          else FullContactList.Insert(index, _Record);

  Result:= _Record;
end;
//------------------------------------------------------------------------------
// returns contact by id
function TContacts.ItemById(id: integer): PContact;
var _Record : PContact;
    index   : integer;
    flag    : boolean;
begin
  Result:= nil;
  index:= 0;
  flag:= false;
  while (index < FullContactList.Count) and not flag do begin
    _Record:= FullContactList.Items[index];
    if _Record^.Id = Id then begin
      Result:= _Record;
      flag:= true;
    end else Inc(index);
  end;
end;
//------------------------------------------------------------------------------
// returns a contact by index from the contact list, for display on the screen
function TContacts.ItemToScreen(index: integer): PContact;
begin
  Result:= ContactListToScreen.Items[index];
end;
//------------------------------------------------------------------------------
// set a filter for the contact list to display on the screen
procedure TContacts.SetContactListFilter(_Family,_Name,_Company,_Email,_Phone: string; _Favorite: boolean);
begin
  with ContactListFilter do begin
    Family:=   _Family;
    Name:=     _Name;
    Company:=  _Company;
    Email:=    _Email;
    Phone:=    _Phone;
    Favorite:= _Favorite;
  end;
end;
//------------------------------------------------------------------------------
// sort the contact list alphabetically
procedure TContacts.SortContactList;
//------------------------------------------------------------------------------
// the function of comparing two strings
  function CompareContacts(Item1, Item2: Pointer): Integer;
  begin
    Result:= CompareText(PContact(Item1).Family, PContact(Item2).Family);
  end;
//------------------------------------------------------------------------------
begin
  FullContactList.Sort(@CompareContacts);
end;
//------------------------------------------------------------------------------
// updating contact data
procedure TContacts.UpdateContact(node: TContact);
var _Record : PContact;
begin
  _Record:= ItemById(node.Id);
  with _Record^ do begin
    Family:=    node.Family;
    Name:=      node.Name;
    DateBirth:= node.DateBirth;
    Company:=   node.Company;
    Email:=     node.Email;
    Phone:=     node.Phone;
    Favorite:=  node.Favorite;
  end;
end;
//------------------------------------------------------------------------------
// update the list of contacts to display according to the filter
procedure TContacts.UpdateListToScreen;
//------------------------------------------------------------------------------
// the extension of the pos function, if the substring is empty, returns true
  function Contains(subStr,Str: string): boolean;
  begin
    if subStr = '' then Result:= true
                   else Result:= pos(subStr,Str) > 0;
  end;
//------------------------------------------------------------------------------
var _Record : PContact;
    index   : integer;
begin
  if ContactListToScreen.Count > 0 then ClearContactListToScreen;
  for index := 0 to FullContactList.Count - 1 do begin
    _Record:= FullContactList.Items[index];
    with ContactListFilter do
      if Contains(Family,_Record^.Family)   and
         Contains(Name,_Record^.Name)       and
         Contains(Company,_Record^.Company) and
         Contains(Email,_Record^.Email)     and
         Contains(Phone,_Record^.Phone)     then
         if not Favorite then ContactListToScreen.Add(Copy(_Record))
                         else
                          if _Record.Favorite then ContactListToScreen.Add(Copy(_Record))
  end;
end;
//------------------------------------------------------------------------------
// clears the contact list
procedure TContacts.ClearFullContactList;
var _Record : PContact;
    index   : integer;
begin
  for index := 0 to FullContactList.Count - 1 do begin
    _Record:= FullContactList.Items[index];
    dispose(_Record);
  end;
  FullContactList.Free;
end;
//------------------------------------------------------------------------------
// clears the auxiliary contact list to display on the screen
procedure TContacts.ClearContactListToScreen;
var _Record : PContact;
    index   : integer;
begin
  for index := 0 to ContactListToScreen.Count - 1 do begin
    _Record:= ContactListToScreen.Items[index];
    dispose(_Record);
  end;
  ContactListToScreen.Clear;
end;
//------------------------------------------------------------------------------
// returns a full copy of the contact
function TContacts.Copy(fromContact: PContact): PContact;
var _RecordToScreen : PContact;
begin
 new(_RecordToScreen);
 with _RecordToScreen^ do begin
    id:=        fromContact^.Id;
    Family:=    fromContact^.Family;
    Name:=      fromContact^.Name;
    DateBirth:= fromContact^.DateBirth;
    Company:=   fromContact^.Company;
    Email:=     fromContact^.Email;
    Phone:=     fromContact^.Phone;
    Favorite:=  fromContact^.Favorite;
    Deleted:=   fromContact^.Deleted;
 end;
 Result:= _RecordToScreen;
end;
//------------------------------------------------------------------------------
end.
