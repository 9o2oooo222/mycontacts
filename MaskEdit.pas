unit MaskEdit;

interface

uses Vcl.Mask, Vcl.Dialogs;

type
  TMaskEdit = class(Vcl.Mask.TMaskEdit)
  protected
    procedure ValidateError; override;
end;

implementation

{ TMaskEdit }

procedure TMaskEdit.ValidateError;
begin
  try
    inherited;
  except
    self.Text:= '';
  end;
end;

end.
